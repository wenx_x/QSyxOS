#ifndef __Q_READYQ_H__
#define __Q_READYQ_H__

#include "QSyxOS.h"
#include "q_bits.h"
#include "q_list.h"

typedef struct {
	QBits    prioMap;
	uint32_t prioArray[CFG_BITS_MAX_SLOT];
	QList    levels[CFG_PRIO_MAX_NUM];
} QRdyQueue;

inline static void Q_RdyQueueInit(QRdyQueue *q)
{
	Q_BitsInit(&q->prioMap, q->prioArray, CFG_BITS_MAX_SLOT);

	int i;
	for (i = 0; i < CFG_PRIO_MAX_NUM; i++) {
		Q_ListInit(&q->levels[i]);
	}
}

inline static void Q_RdyQueueInsert(QRdyQueue *q, QTask *task)
{
	Q_ListAppend(&q->levels[task->prio], &task->taskNode);
	Q_BitsSet(&q->prioMap, task->prio);
}

inline static void Q_RdyQueueRemove(QRdyQueue *q, QTask *task)
{
	Q_ListRemove(&task->taskNode);
	if (Q_ListEmpty(&q->levels[task->prio]))
		Q_BitsClr(&q->prioMap, task->prio);
}

inline static QTask *Q_RdyQueueGetHighest(QRdyQueue *q)
{
	int highest = Q_BitsGetHighest(&q->prioMap);
	if (highest < 0)
		return NULL;
	return Q_ListFirstEntry(&q->levels[highest], QTask, taskNode);
}

#endif	/* __Q_READYQ_H__ */
