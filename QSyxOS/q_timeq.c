#include "QSyxOS.h"
#include "q_list.h"
#include "q_timeq.h"
#include "q_config.h"

void Q_TimeQueueInit(QTimeQueue *timeq)
{
	int i;
	for (i = 0; i < CFG_TIMEWHEEL_MAX_SPOKE; i++) {
		Q_ListInit(&timeq->wheel[i]);
	}
}

void Q_TimeQueueInsert(QTimeQueue *timeq, QTimeNode *node)
{
	int spoke = node->tickLimits % CFG_TIMEWHEEL_MAX_SPOKE;
	QList *wheel = &timeq->wheel[spoke];

	QListNode *n;
	for (n = Q_ListFirst(wheel); n; n = Q_ListNext(wheel, n)) {
		QTimeNode *node1 = Q_ListEntry(n, QTimeNode, tickHead);
		if (node1->tickLimits > node->tickLimits) {
			Q_ListInsertBefore(&node1->tickHead, &node->tickHead);
			break;
		}
	}

	if (n == NULL)
		Q_ListAppend(wheel, &node->tickHead);
}

void Q_TimeQueueRemove(QTimeQueue *timeq, QTimeNode *node)
{
	(void)timeq;
	Q_ListRemove(&node->tickHead);
}

int Q_TimeQueueUpdate(QTimeQueue *timeq, uint32_t tick)
{
	QList *spoke = &timeq->wheel[tick % CFG_TIMEWHEEL_MAX_SPOKE];

	QListNode *n, *n_next;
	for (n = Q_ListFirst(spoke); n; n = n_next) {
		n_next = Q_ListNext(spoke, n);
		QTimeNode *node = Q_ListEntry(n, QTimeNode, tickHead);

		if (node->tickLimits == tick) {
			Q_ListRemove(&node->tickHead);

			if (node->func)
				node->func(node->ctx);

		} else if (node->tickLimits > tick)
			break;
	}

	return 0;
}
