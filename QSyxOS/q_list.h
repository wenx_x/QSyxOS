#ifndef __Q_LIST_H__
#define __Q_LIST_H__

#include "QSyxOS.h"

#define container_of(ptr, type, member) 	(type *)((char *)ptr - offsetof(type, member))

inline static void Q_ListInit(QList *list)
{
	list->head.next = &list->head;
	list->head.prev = &list->head;
}

inline static int Q_ListEmpty(QList *list)
{
	return (list->head.next == &list->head);
}

inline static void Q_ListAppend(QList *list, QListNode *node)
{
	node->next = &list->head;
	node->prev = list->head.prev;
	list->head.prev->next = node;
	list->head.prev = node;
}

inline static void Q_ListPrepend(QList *list, QListNode *node)
{
	node->next = list->head.next;
	node->prev = &list->head;
	list->head.next->prev = node;
	list->head.next = node;
}

inline static void Q_ListRemove(QListNode *node)
{
	if (node->next == NULL)
		return;
	node->next->prev = node->prev;
	node->prev->next = node->next;
	node->next = NULL;
	node->prev = NULL;
}

inline static void Q_ListInsertAfter(QListNode *head, QListNode *node)
{
	node->next = head->next;
	node->prev = head;
	head->next->prev = node;
	head->next = node;
}

inline static void Q_ListInsertBefore(QListNode *head, QListNode *node)
{
	node->next = head;
	node->prev = head->prev;
	head->prev->next = node;
	head->prev = node;
}

inline static QListNode *Q_ListFirst(QList *list)
{
	if (Q_ListEmpty(list))
		return NULL;
	return list->head.next;
}

inline static QListNode *Q_ListLast(QList *list)
{
	if (Q_ListEmpty(list))
		return NULL;
	return list->head.prev;
}

inline static int Q_ListIsLast(QList *list, QListNode *node)
{
	return node->next == &list->head;
}

inline static QListNode *Q_ListNext(QList *list, QListNode *node)
{
	if (Q_ListEmpty(list))
		return NULL;
	if (Q_ListIsLast(list, node))
		return NULL;
	return node->next;
}

/**
 * Q_ListEntry - get the struct for this entry
 * @ptr:	the &QListNode pointer.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the QListNode within the struct.
 */
#define Q_ListEntry(ptr, type, member) \
	container_of(ptr, type, member)

/**
 * Q_ListFirstEntry - get the first element from a list
 * @ptr:	the QList to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the QListNode within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#define Q_ListFirstEntry(list, type, member) \
	Q_ListEntry((list)->head.next, type, member)

/**
 * Q_ListNextEntry - get the next element in list
 * @pos:	the type * to cursor
 * @member:	the name of the QListNode within the struct.
 */
#define Q_ListNextEntry(pos, type, member) \
	Q_ListEntry((pos)->member.next, type, member)

#endif /* __Q_LIST_H__ */
