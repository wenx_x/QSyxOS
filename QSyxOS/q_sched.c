#include "QSyxOS.h"
#include "q_task.h"
#include "q_port.h"
#include "q_sched.h"

QTask   *QTaskTickPtr  = NULL;
uint32_t QSchedLockCtr = 0;

int QSchedStarted = 0;

void Q_SchedSwitch(void)
{
	if (QSchedStarted == 0)
		return;

	if (QTaskTickPtr) {
		QTaskNextPtr = QTaskTickPtr;
		QTaskTickPtr = NULL;
	} else {
		QTaskNextPtr = Q_TaskGetRdyHighest();
	}

	if (QTaskNextPtr && QTaskCurPtr != QTaskNextPtr)
		QTaskCtxSwitch();
}

void QSchedStart(void)
{
	CPU_REG reg;
	CPU_CRITICAL_ENTER(reg);
	if (QSchedStarted == 0) {
		QSchedStarted = 1;
		Q_SchedSwitch();
	}
	CPU_CRITICAL_EXIT(reg);
}

void QSchedSwitch(void)
{
	CPU_REG reg;
	CPU_CRITICAL_ENTER(reg);
	Q_SchedSwitch();
	CPU_CRITICAL_EXIT(reg);
}

void QSchedLock(void)
{
	CPU_REG reg;
	CPU_CRITICAL_ENTER(reg);
	QSchedLockCtr++;
	CPU_CRITICAL_EXIT(reg);
}

void QSchedUnlock(void)
{
	CPU_REG reg;
	CPU_CRITICAL_ENTER(reg);
	if (--QSchedLockCtr == 0)
		Q_SchedSwitch();
	CPU_CRITICAL_EXIT(reg);
}

void QSchedUnlockNoSched(void)
{
	CPU_REG reg;
	CPU_CRITICAL_ENTER(reg);
	QSchedLockCtr--;
	CPU_CRITICAL_EXIT(reg);
}
