#include "QSyxOS.h"
#include "q_task.h"
#include "q_poll.h"
#include "q_pipe.h"
#include "q_sched.h"
#include "q_pendq.h"

#define min(a, b)	((a) < (b) ? (a) : (b))

int QPipeInit(QPipe *pipe, void *buf, int size)
{
	pipe->size = size;
	pipe->used = 0;

	pipe->buf = buf;
	pipe->end = buf + size;

	pipe->wptr = buf;
	pipe->rptr = buf;

	Q_PendQueueInit(&pipe->pendQ);
#ifdef CFG_QPOLL_ENABLE
	Q_PollQueueInit(&pipe->pollQ);
#endif

	return 0;
}

inline static int Q_PipeGet(QPipe *pipe, void *buf, int size)
{
	int total = min(Q_PipeUsed(pipe), size);
	int left = total;

	do {
		int nbyte = min(left, pipe->end - pipe->rptr);
		memcpy(buf, pipe->rptr, nbyte);
		buf += nbyte;
		pipe->rptr += nbyte;
		if (pipe->rptr == pipe->end)
			pipe->rptr = pipe->buf;
		left -= nbyte;
	} while (left > 0);

	pipe->used -= total;

	return total;
}

inline static int Q_PipePut(QPipe *pipe, void *buf, int size)
{
	int total = min(Q_PipeFree(pipe), size);
	int left = total;

	do {
		int nbyte = min(left, pipe->end - pipe->wptr);
		memcpy(pipe->wptr, buf, nbyte);
		buf += nbyte;
		pipe->wptr += nbyte;
		if (pipe->wptr == pipe->end)
			pipe->wptr = pipe->buf;
		left -= nbyte;
	} while (left > 0);

	pipe->used += total;

	return total;
}

int QPipeGet(QPipe *pipe, void *buf, int size, int timeout)
{
	int nread = 0;
	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();
	if (!Q_PipeEmpty(pipe)) {
		nread = Q_PipeGet(pipe, buf, size);

		QTask *task = Q_PendQueueFirst(&pipe->pendQ);
		if (task) {
#ifdef CFG_QPOLL_ENABLE
			QPollEvent *ev = Q_PollQueueGetHighest(&pipe->pollQ);
			if (ev && ev->poller->prio > task->prio) {
				Q_PollSignal(ev, QPollFlag_Write);
			} else
#endif
			{
				int nwrite = Q_PipePut(pipe, task->swapBuf, task->swapSize);
				task->swapSize -= nwrite;
				Q_PendQueuePost(&pipe->pendQ, task);
			}
#ifdef CFG_QPOLL_ENABLE
		} else {
			QPollEvent *ev = Q_PollQueueGetHighest(&pipe->pollQ);
			if (ev)
				Q_PollSignal(ev, QPollFlag_Write);
#endif
		}

		QSchedUnlock();
		return nread;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return -1;
	}

	QTaskCurPtr->swapBuf = buf;
	QTaskCurPtr->swapSize = size;
	if (Q_PendQueuePend(&pipe->pendQ, timeout, 1) < 0) {
		QSchedUnlock();
		return -1;
	}
	nread = size - QTaskCurPtr->swapSize;
	QSchedUnlock();

	return nread;
}

int QPipePut(QPipe *pipe, void *buf, int size, int timeout)
{
	int nwrite = 0;

	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();
	if (!Q_PipeFull(pipe)) {
		QTask *task = Q_PendQueueFirst(&pipe->pendQ);
		if (task) {
#ifdef CFG_QPOLL_ENABLE
			QPollEvent *ev = Q_PollQueueGetHighest(&pipe->pollQ);
			if (ev && ev->poller->prio > task->prio) {
				nwrite = Q_PipePut(pipe, buf, size);
				Q_PollSignal(ev, QPollFlag_Read);
			} else
#endif
			{
				nwrite = min(size, task->swapSize);
				memcpy(task->swapBuf, buf, nwrite);
				task->swapSize -= nwrite;
				Q_PendQueuePost(&pipe->pendQ, task);
			}
#ifdef CFG_QPOLL_ENABLE
		} else {
			nwrite = Q_PipePut(pipe, buf, size);
			QPollEvent *ev = Q_PollQueueGetHighest(&pipe->pollQ);
			if (ev)
				Q_PollSignal(ev, QPollFlag_Read);
#endif
		}
		QSchedUnlock();
		return nwrite;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return -1;
	}

	QTaskCurPtr->swapBuf = buf;
	QTaskCurPtr->swapSize = size;
	if (Q_PendQueuePend(&pipe->pendQ, timeout, 1) < 0) {
		QSchedUnlock();
		return -1;
	}
	nwrite = size - QTaskCurPtr->swapSize;
	QSchedUnlock();

	return nwrite;
}

#ifdef CFG_QPOLL_ENABLE
int Q_PipePollEvent(QPipe *pipe, QPollEvent *ev)
{
	int needwait = 0;
	switch (ev->flag) {
	case QPollFlag_Read:
		needwait = Q_PipeEmpty(pipe);
		break;
	case QPollFlag_Write:
		needwait = Q_PipeFull(pipe);
		break;
	}

	if (needwait) {
		Q_PollQueueInsert(&pipe->pollQ, ev);
	} else {
		ev->ready = 1;
		ev->poller->numReadys++;
	}

	return 0;
}
#endif
