#ifndef __Q_SCHED_H__
#define __Q_SCHED_H__

#include "QSyxOS.h"

extern QTask   *QTaskTickPtr;
extern uint32_t QSchedLockCtr;
extern int QSchedStarted;

void QSchedStart(void);

#define Q_SchedRunning() (QSchedStarted)

void Q_SchedSwitch(void);
void QSchedSwitch(void);
void QSchedLock(void);
void QSchedUnlock(void);
void QSchedUnlockNoSched(void);

#endif /* __Q_SCHED_H__ */
