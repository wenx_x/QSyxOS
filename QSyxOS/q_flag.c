#include "QSyxOS.h"
#include "q_task.h"
#include "q_sched.h"
#include "q_pendq.h"

int QFlagInit(QFlag *flag)
{
	Q_PendQueueInit(&flag->pendQ);
	flag->flags = 0;
	return 0;
}

int QFlagPend(QFlag *flag, int mask, QFlagPendOpt opt, int timeout)
{
	Q_ERRNO = Q_ERR_NONE;

	if (mask & 0x80000000) {
		Q_ERRNO = Q_ERR_FLAG_INVALID_MASK;
		return -1;
	}

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();
	int ready = flag->flags & mask;

	switch (opt) {
	case QFlagPendOpt_Set_All:
		if (ready == mask) {
			flag->flags &= ~ready;
			QSchedUnlock();
			return ready;
		}
		break;
	case QFlagPendOpt_Set_Any:
		if (ready != 0) {
			flag->flags &= ~ready;
			QSchedUnlock();
			return ready;
		}
		break;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return -1;
	}

	flag->flags &= ~ready;

	QTaskCurPtr->flagsOpt  = opt;
	QTaskCurPtr->flagsRdy  = 0;
	QTaskCurPtr->flagsPend = mask & ~ready;

	if (Q_PendQueuePend(&flag->pendQ, timeout, 1) < 0) {
		QSchedUnlock();
		return -1;
	}

	uint32_t flags = QTaskCurPtr->flagsRdy;
	QSchedUnlock();

	return flags;
}

static int wakeupTasks(QTask *task, QFlag *flag)
{
	int ready = flag->flags & task->flagsPend;
	if (!ready)
		return 0;

	task->flagsPend &= ~ready;
	task->flagsRdy  |=  ready;
	flag->flags     &= ~ready;

	switch (task->flagsOpt) {
	case QFlagPendOpt_Set_All:
		if (task->flagsPend == 0)
			return 1;
		break;
	case QFlagPendOpt_Set_Any:
		if (ready != 0)
			return 1;
		break;
	}

	return 0;
}

int QFlagPost(QFlag *flag, int mask)
{
	Q_ERRNO = Q_ERR_NONE;

	if (mask & 0x80000000) {
		Q_ERRNO = Q_ERR_FLAG_INVALID_MASK;
		return -1;
	}

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();
	flag->flags |= mask;

	QTask *task, *task_next;
	for (task = Q_PendQueueFirst(&flag->pendQ); task; task = task_next) {
		task_next = Q_PendQueueNext(&flag->pendQ, task);

		if (wakeupTasks(task, flag) == 1)
			Q_PendQueuePost(&flag->pendQ, task);
	}

	QSchedUnlock();
	return 0;
}
