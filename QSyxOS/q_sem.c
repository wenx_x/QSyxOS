#include "QSyxOS.h"
#include "q_task.h"
#include "q_sched.h"
#include "q_pendq.h"

int QSemInit(QSem *sem, uint32_t count)
{
	Q_PendQueueInit(&sem->pendQ);
	sem->count = count;
	return 0;
}

int QSemPend(QSem *sem, int timeout)
{
	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();

	if (sem->count > 0) {
		sem->count--;
		QSchedUnlock();
		return 0;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return -1;
	}

	if (Q_PendQueuePend(&sem->pendQ, timeout, 1) < 0) {
		QSchedUnlock();
		return -1;
	}

	QSchedUnlock();

	return 0;
}

int QSemPost(QSem *sem)
{
	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();

	QTask *task = Q_PendQueueFirst(&sem->pendQ);
	if (task)
		Q_PendQueuePost(&sem->pendQ, task);
	else
		sem->count++;

	QSchedUnlock();

	return 0;
}
