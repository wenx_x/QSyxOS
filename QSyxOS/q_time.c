#include "QSyxOS.h"
#include "q_task.h"
#include "q_tick.h"
#include "q_time.h"
#include "q_sched.h"

static void timeout(void *ctx)
{
	Q_TaskClrState((QTask *)ctx, QTaskState_Waiting);
}

static void Q_TimePend(int ticks)
{
	QSchedLock();
	Q_TickPend(&QTaskCurPtr->tickNode, ticks, timeout, QTaskCurPtr);
	Q_TaskSetState(QTaskCurPtr, QTaskState_Waiting);
	QSchedUnlock();
}

static void Q_TimePost(QTask *task)
{
	QSchedLock();
	if (Q_TaskInState(task, QTaskState_Waiting)) {
		Q_TickPost(&task->tickNode);
		Q_TaskClrState(task, QTaskState_Waiting);
	}
	QSchedUnlock();
}

int QTimeDelay(int ticks)
{
	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	Q_TimePend(ticks);
	return 0;
}

int QTimeAbort(QTask *task)
{
	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	Q_TimePost(task);
	return 0;
}

void Q_TimeUpdatePrio(QTask *task, int prio)
{
	task->prio = prio;
}
