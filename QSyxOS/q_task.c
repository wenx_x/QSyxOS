#include "QSyxOS.h"
#include "q_list.h"
#include "q_task.h"
#include "q_tick.h"
#include "q_time.h"
#include "q_port.h"
#include "q_pendq.h"
#include "q_sched.h"
#include "q_config.h"
#include "q_readyq.h"

static QTask    idleTcb;
static uint32_t idleStk[256];

QTask *QTaskCurPtr  = &idleTcb;
QTask *QTaskNextPtr = NULL;

static void idleTsk(void *arg)
{
	(void)arg;
	while (1) {
		/* TODO: do something */
	}
}

static QRdyQueue rdyTbl;

void Q_TaskInitModule(void)
{
	Q_RdyQueueInit(&rdyTbl);
	QTaskCreate(&idleTcb, "QSyxOS Idle Task", CFG_TASK_PRIO_IDLE, (uint8_t *)idleStk, sizeof(idleStk), 10, idleTsk, NULL);
}

void Q_TaskSetRdy(QTask *task)
{
	Q_RdyQueueInsert(&rdyTbl, task);
}

void Q_TaskDelRdy(QTask *task)
{
	Q_RdyQueueRemove(&rdyTbl, task);
}

QTask *Q_TaskGetRdyHighest(void)
{
	return Q_RdyQueueGetHighest(&rdyTbl);
}

void Q_TaskYield(QTask *task)
{
	Q_RdyQueueRemove(&rdyTbl, task);
	Q_RdyQueueInsert(&rdyTbl, task);
}

#define valid_prio(n)	((n) < CFG_PRIO_LOWEST && (n) > CFG_PRIO_HIGHEST)

int Q_TaskInit(QTask *task, const char *name, int prio, uint8_t *stk_ptr,
		int stk_size, int time_quanta, void (*entry)(void *arg), void *arg)
{
	if (task != &idleTcb && !valid_prio(prio)) {
		Q_ERRNO = Q_ERR_TASK_INVALID_PRIO;
		return -1;
	}

	task->taskState  = QTaskState_Ready;
	task->StkPtr     = QTaskInitStack(stk_ptr, stk_size, entry, arg);
	task->name       = name;
	task->StkSize    = stk_size;
	task->entry      = entry;
	task->prio       = prio;
	task->originPrio = prio;
	task->tickQuanta = time_quanta > 0 ? time_quanta : CFG_TIMEQUANTA_DEFAULT;
	task->tickRemain = task->tickQuanta;
	task->flagsPend  = 0;
	task->flagsRdy   = 0;
	task->flagsOpt   = 0;

	task->pendState  = QPendState_Success;
	task->pendQueue  = NULL;

	Q_TickInit(&task->tickNode);

	return 0;
}

int QTaskCreate(QTask *task, const char *name, int prio, void *stk_ptr,
		int stk_size, int time_quanta, void (*entry)(void *arg), void *arg)
{
	if (Q_TaskInit(task, name, prio, stk_ptr, stk_size, time_quanta, entry, arg) < 0)
		return -1;

	CPU_REG reg;
	CPU_CRITICAL_ENTER(reg);
	Q_TaskSetRdy(task);
	if (Q_SchedRunning())
		Q_SchedSwitch();
	CPU_CRITICAL_EXIT(reg);

	return 0;
}

int Q_TaskUpdatePrio(QTask *task, int prio)
{
	if (task->taskState == QTaskState_Ready) {
		Q_TaskDelRdy(task);
		task->prio = prio;
		Q_TaskSetRdy(task);
	}

	if (task->taskState & QTaskState_Waiting) {
		Q_TimeUpdatePrio(task, prio);
	}

	if (task->taskState & QTaskState_Pending) {
		Q_PendQueueUpdatePrio(task, prio);
	}

	return 0;
}

/* Task Round Robin */
void Q_TaskUpdate(void)
{
	QTask *highest = Q_TaskGetRdyHighest();

	if (highest->tickQuanta == 0)
		return;

	highest->tickRemain--;
	if (highest->tickRemain != 0)
		return;

	Q_TaskYield(highest);

	highest->tickRemain = highest->tickQuanta;
}

int Q_TaskSetState(QTask *task, int state)
{
	int newstate = task->taskState | state;
	if (newstate == task->taskState) {
		Q_ERRNO = Q_ERR_TASK_INVALID_STATE;
		return -1;
	}

	if (task->taskState == QTaskState_Ready)
		Q_TaskDelRdy(task);

	task->taskState = (QTaskState)newstate;

	return 0;
}

int Q_TaskClrState(QTask *task, int state)
{
	int newstate = task->taskState & (~state);
	if (newstate == task->taskState) {
		Q_ERRNO = Q_ERR_TASK_INVALID_STATE;
		return -1;
	}

	task->taskState = (QTaskState)newstate;
	if (task->taskState == QTaskState_Ready)
		Q_TaskSetRdy(task);

	return 0;
}

int QTaskSuspend(QTask *task)
{
	QSchedLock();
	Q_TaskSetState(task, QTaskState_Suspend);
	QSchedUnlock();
	return 0;
}

int QTaskResume(QTask *task)
{
	QSchedLock();
	Q_TaskClrState(task, QTaskState_Suspend);
	QSchedUnlock();
	return 0;
}

void QTaskYield(void)
{
	QSchedLock();
	Q_TaskYield(QTaskCurPtr);
	QSchedUnlock();
}
