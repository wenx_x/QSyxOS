#include "QSyxOS.h"
#include "q_list.h"
#include "q_tick.h"
#include "q_msgq.h"
#include "q_mbox.h"
#include "q_pipe.h"
#include "q_task.h"
#include "q_sched.h"
#include "q_queue.h"

#ifdef CFG_QPOLL_ENABLE

static void timeout(void *ctx)
{
	QTask *task = (QTask *)ctx;
	task->pendState = QPendState_Timeout;
	Q_TaskClrState(task, QTaskState_Pending | QTaskState_Waiting);
}

int Q_PollQueueInsert(QPollQueue *pollQ, QPollEvent *ev)
{
	QListNode *n;
	for (n = Q_ListFirst(&pollQ->eventList); n; n = Q_ListNext(&pollQ->eventList, n)) {
		QPollEvent *ev1 = Q_ListEntry(n, QPollEvent, listNode);
		if (Q_TaskPrioIsHigher(ev->poller->prio, ev1->poller->prio)) {
			Q_ListInsertBefore(&ev1->listNode, &ev->listNode);
			break;
		}
	}

	if (n == NULL)
		Q_ListAppend(&pollQ->eventList, &ev->listNode);

	return 0;
}

inline static void Q_PollQueueRemove(QPollEvent *ev)
{
	Q_ListRemove(&ev->listNode);
}

void Q_PollQueueInit(QPollQueue *pollQ)
{
	Q_ListInit(&pollQ->eventList);
}

QPollEvent *Q_PollQueueGetHighest(QPollQueue *pollQ)
{
	if (Q_ListEmpty(&pollQ->eventList))
		return NULL;

	return Q_ListFirstEntry(&pollQ->eventList, QPollEvent, listNode);
}

inline static void cleanUp(QPollEvent *events, int numEvents)
{
	int i;
	for (i = 0; i < numEvents; i++) {
		if (events[i].ready == 0)
			Q_PollQueueRemove(&events[i]);
	}
}

int QPoll(QPollEvent *events, int numEvents, int ticks)
{
	QSchedLock();

	Q_ERRNO = Q_ERR_NONE;

	QTask *task = QTaskCurPtr;
	task->numReadys = 0;

	int i;
	for (i = 0; i < numEvents; i++) {
		QPollEvent *ev = &events[i];
		ev->ready = 0;
		ev->poller = task;
		switch (ev->type) {
		case QPollObjType_MsgQ:
			Q_MsgQPollEvent(ev->kobj.msgq, ev);
			break;
		case QPollObjType_Mbox:
			Q_MboxPollEvent(ev->kobj.mbox, ev);
			break;
		case QPollObjType_Pipe:
			Q_PipePollEvent(ev->kobj.pipe, ev);
			break;
		case QPollObjType_Queue:
			Q_QueuePollEvent(ev->kobj.queue, ev);
			break;
		}
	}

	if (task->numReadys > 0) {
		cleanUp(events, numEvents);
		QSchedUnlock();
		return task->numReadys;
	}

	if (ticks > 0) {
		Q_TickPend(&task->tickNode, ticks, timeout, task);
		Q_TaskSetState(task, QTaskState_Pending | QTaskState_Waiting);
	} else {
		Q_TaskSetState(task, QTaskState_Pending);
	}

	QSchedUnlockNoSched();

	QSchedSwitch();

	QSchedLock();
	if (task->pendState == QPendState_Timeout) {
		cleanUp(events, numEvents);
		Q_ERRNO = Q_ERR_TIMEOUT;
		QSchedUnlock();
		return -1;
	}

	cleanUp(events, numEvents);
	QSchedUnlock();

	return task->numReadys;
}

int Q_PollSignal(QPollEvent *event, QPollFlag flag)
{
	Q_ERRNO = Q_ERR_NONE;

	if ((event->flag & flag) == 0)
		return 0;

	QTask *task = event->poller;
	if (task != NULL) {
		if (Q_TaskInState(task, QTaskState_Waiting)) {
			Q_TickPost(&task->tickNode);
			Q_TaskClrState(task, QTaskState_Waiting);
		}

		if (Q_TaskInState(task, QTaskState_Pending))
			Q_TaskClrState(task, QTaskState_Pending);

		event->ready = 1;
		event->poller = NULL;

		task->numReadys++;
		Q_PollQueueRemove(event);
	}

	return 0;
}

#endif /* CFG_QPOLL_ENABLE */
