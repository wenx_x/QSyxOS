#ifndef __Q_TIMEQ_H__
#define __Q_TIMEQ_H__

#include "QSyxOS.h"
#include "q_config.h"

typedef struct {
	QList wheel[CFG_TIMEWHEEL_MAX_SPOKE];
} QTimeQueue;

void Q_TimeQueueInit(QTimeQueue *timeq);

void Q_TimeQueueInsert(QTimeQueue *timeq, QTimeNode *node);
void Q_TimeQueueRemove(QTimeQueue *timeq, QTimeNode *node);

int Q_TimeQueueUpdate(QTimeQueue *timeq, uint32_t tick);

#endif /* __Q_TIMEQ_H__ */
