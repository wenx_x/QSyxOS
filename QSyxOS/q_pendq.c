#include "QSyxOS.h"
#include "q_task.h"
#include "q_tick.h"
#include "q_sched.h"
#include "q_pendq.h"

void Q_PendQueueInit(QPendQueue *q)
{
	q->numTasks = 0;
	Q_ListInit(&q->taskLists);
}

static int Q_PendQueueInsert(QPendQueue *q, QTask *task)
{
	QListNode *n;
	for (n = Q_ListFirst(&q->taskLists); n; n = Q_ListNext(&q->taskLists, n)) {
		QTask *tsk1 = Q_ListEntry(n, QTask, taskNode);
		if (Q_TaskPrioIsHigher(task->prio, tsk1->prio)) {
			Q_ListInsertBefore(&tsk1->taskNode, &task->taskNode);
			break;
		}
	}

	if (n == NULL)
		Q_ListAppend(&q->taskLists, &task->taskNode);

	q->numTasks++;

	return 0;
}

static int Q_PendQueueRemove(QPendQueue *q, QTask *task)
{
	Q_ListRemove(&task->taskNode);
	q->numTasks--;
	return 0;
}

static void timeout(void *ctx)
{
	QTask *task = (QTask *)ctx;

	Q_PendQueueRemove(task->pendQueue, task);
	task->pendState = QPendState_Timeout;
	task->pendQueue = NULL;

	Q_TaskClrState(task, QTaskState_Pending | QTaskState_Waiting);
}

int Q_PendQueuePend(QPendQueue *q, int ticks, int schedLocked)
{
	QTask *task = QTaskCurPtr;

	if (ticks > 0) {
		Q_TickPend(&task->tickNode, ticks, timeout, task);
		Q_TaskSetState(task, QTaskState_Pending | QTaskState_Waiting);
	} else {
		Q_TaskSetState(task, QTaskState_Pending);
	}

	Q_PendQueueInsert(q, task);
	task->pendState = QPendState_Started;
	task->pendQueue = q;

	if (schedLocked)
		QSchedUnlockNoSched();

	QSchedSwitch();

	if (schedLocked)
		QSchedLock();

	if (task->pendState == QPendState_Timeout) {
		Q_ERRNO = Q_ERR_TIMEOUT;
		return -1;
	}

	return 0;
}

int Q_PendQueuePost(QPendQueue *q, QTask *task)
{
	Q_PendQueueRemove(q, task);

	task->pendState = QPendState_Success;
	task->pendQueue = NULL;

	if (Q_TaskInState(task, QTaskState_Waiting)) {
		Q_TickPost(&task->tickNode);
		Q_TaskClrState(task, QTaskState_Pending | QTaskState_Waiting);
	} else {
		Q_TaskClrState(task, QTaskState_Pending);
	}

	return 0;
}

int Q_PendQueueUpdatePrio(QTask *task, int prio)
{
	QPendQueue *q = task->pendQueue;
	if (!q)
		return -1;

	Q_PendQueueRemove(q, task);
	task->prio = prio;
	Q_PendQueueInsert(q, task);

	return 0;
}
