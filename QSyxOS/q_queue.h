#ifndef __Q_QUEUE_H__
#define __Q_QUEUE_H__

#define Q_QueueEmpty(queue)	Q_ListEmpty(&(queue)->allnodes)

#ifdef CFG_QPOLL_ENABLE
int Q_QueuePollEvent(QQueue *queue, QPollEvent *ev);
#endif

#endif /* __Q_QUEUE_H__ */
