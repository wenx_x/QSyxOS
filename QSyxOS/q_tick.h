#ifndef __Q_TICK_H__
#define __Q_TICK_H__

#include "QSyxOS.h"

void Q_TickInitModule(void);

void Q_TickInit(QTick *tick);

void Q_TickPend(QTick *tick, int ticks, void (*func)(void *ctx), void *ctx);
void Q_TickPost(QTick *tick);

void Q_TickUpdate(void);

#endif /* __Q_TICK_H__ */
