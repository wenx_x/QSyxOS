#ifndef __Q_PORT_H__
#define __Q_PORT_H__

#include "QSyxOS.h"
#include "q_config.h"

extern uint32_t CPU_IRQ_DISABLE(void);
extern void CPU_IRQ_ENABLE(uint32_t reg);

static inline CPU_REG IrqDisable(void)
{
	return CPU_IRQ_DISABLE();
}

static inline void IrqEnable(CPU_REG reg)
{
	CPU_IRQ_ENABLE(reg);
}

#define NVIC_INT_CTRL *(uint32_t *)0xE000ED04
#define NVIC_PENDSVSET 0x10000000
#define QTaskCtxSwitch()	do { NVIC_INT_CTRL = NVIC_PENDSVSET; } while (0)

__asm inline static int CPU_CntLeadZeros(uint32_t word)
{
	CLZ R0, R0
	BX LR
}

static inline int QBitsCountLeadingZero(uint32_t word)
{
	int i;
	for (i = CFG_BITS_PER_UINT32 - 1; i >= 0; i--) {
		if (word & (1 << i)) {
			return CFG_BITS_PER_UINT32 - i - 1;
		}
	}
	return -1;
}

void *QTaskInitStack(uint8_t *stk_ptr, int stk_size, void (*task)(void *arg), void *arg);

void QSysTickInit(void);

extern QTask    QSysTickTcb;
extern uint32_t QSysTickDlyCtr;

#endif /* __Q_PORT_H__ */
