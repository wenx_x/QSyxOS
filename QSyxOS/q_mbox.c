#include "QSyxOS.h"
#include "q_task.h"
#include "q_mbox.h"
#include "q_poll.h"
#include "q_sched.h"
#include "q_pendq.h"
#include "q_config.h"

int QMboxInit(QMbox *mbox, void *buf, int nb_blks, int blksize)
{
	mbox->total = nb_blks;
	mbox->used  = 0;
	mbox->buf   = buf;
	mbox->end   = (uint8_t *)buf + nb_blks * blksize;
	mbox->wptr  = buf;
	mbox->rptr  = buf;
	mbox->blksize = blksize;

	Q_PendQueueInit(&mbox->pendQ);
#ifdef CFG_QPOLL_ENABLE
	Q_PollQueueInit(&mbox->pollQ);
#endif
	return 0;
}

inline static void Q_MboxPut(QMbox *mbox, void *buf)
{
	memcpy(mbox->wptr, buf, mbox->blksize);
	mbox->wptr += mbox->blksize;

	if (mbox->wptr == mbox->end)
		mbox->wptr = mbox->buf;

	mbox->used++;
}

int QMboxPut(QMbox *mbox, void *buf, int timeout)
{
	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();
	if (!Q_MboxFull(mbox)) {
		QTask *task = Q_PendQueueFirst(&mbox->pendQ);
		if (task) {
#ifdef CFG_QPOLL_ENABLE
			QPollEvent *ev = Q_PollQueueGetHighest(&mbox->pollQ);
			if (ev && ev->poller->prio > task->prio) {
				Q_MboxPut(mbox, buf);
				Q_PollSignal(ev, QPollFlag_Read);
			} else
#endif
			{
				memcpy(task->swapBuf, buf, mbox->blksize);
				Q_PendQueuePost(&mbox->pendQ, task);
			}
#ifdef CFG_QPOLL_ENABLE
		} else {
			Q_MboxPut(mbox, buf);
			QPollEvent *ev = Q_PollQueueGetHighest(&mbox->pollQ);
			if (ev)
				Q_PollSignal(ev, QPollFlag_Read);
#endif
		}
		QSchedUnlock();
		return 0;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return -1;
	}

	QTaskCurPtr->swapBuf = buf;
	if (Q_PendQueuePend(&mbox->pendQ, timeout, 1) < 0) {
		QSchedUnlock();
		return -1;
	}

	QSchedUnlock();

	return 0;
}

inline static void Q_MboxGet(QMbox *mbox, void *buf)
{
	memcpy(buf, mbox->rptr, mbox->blksize);
	mbox->rptr += mbox->blksize;

	if (mbox->rptr == mbox->end)
		mbox->rptr = mbox->buf;

	mbox->used--;
}

int QMboxGet(QMbox *mbox, void *buf, int timeout)
{
	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();
	if (!Q_MboxEmpty(mbox)) {
		Q_MboxGet(mbox, buf);

		QTask *task = Q_PendQueueFirst(&mbox->pendQ);
		if (task) {
#ifdef CFG_QPOLL_ENABLE
			QPollEvent *ev = Q_PollQueueGetHighest(&mbox->pollQ);
			if (ev && ev->poller->prio > task->prio) {
				Q_PollSignal(ev, QPollFlag_Write);
			} else
#endif
			{
				Q_MboxPut(mbox, task->swapBuf);
				Q_PendQueuePost(&mbox->pendQ, task);
			}
#ifdef CFG_QPOLL_ENABLE
		} else {
			QPollEvent *ev = Q_PollQueueGetHighest(&mbox->pollQ);
			if (ev)
				Q_PollSignal(ev, QPollFlag_Write);
#endif
		}
		QSchedUnlock();
		return 0;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return -1;
	}

	QTaskCurPtr->swapBuf = buf;
	if (Q_PendQueuePend(&mbox->pendQ, timeout, 1) < 0) {
		QSchedUnlock();
		return -1;
	}

	QSchedUnlock();

	return 0;
}

#ifdef CFG_QPOLL_ENABLE
int Q_MboxPollEvent(QMbox *mbox, QPollEvent *ev)
{
	int needwait = 0;
	switch (ev->flag) {
	case QPollFlag_Read:
		needwait = Q_MboxEmpty(mbox);
		break;
	case QPollFlag_Write:
		needwait = Q_MboxFull(mbox);
		break;
	}

	if (needwait) {
		Q_PollQueueInsert(&mbox->pollQ, ev);
	} else {
		ev->ready = 1;
		ev->poller->numReadys++;
	}

	return 0;
}
#endif
