#ifndef __Q_TASK_H__
#define __Q_TASK_H__

#include "QSyxOS.h"

extern QTask *QTaskCurPtr;
extern QTask *QTaskNextPtr;

void Q_TaskInitModule(void);

int Q_TaskUpdatePrio(QTask *task, int prio);

void Q_TaskUpdate(void);

QTask *Q_TaskGetRdyHighest(void);

int Q_TaskInit(QTask *task, const char *name, int prio, uint8_t *stk_ptr,
		int stk_size, int time_quanta, void (*entry)(void *arg), void *arg);

int Q_TaskSetState(QTask *task, int state);
int Q_TaskClrState(QTask *task, int state);

void Q_TaskSetRdy(QTask *task);
void Q_TaskDelRdy(QTask *task);

#define Q_TaskPrioIsHigher(p1, p2)	((p1) < (p2))	/* smaller number has higher priority */

#define Q_TaskInState(task, st) (task->taskState & st)

#endif /* __Q_TASK_H__ */
