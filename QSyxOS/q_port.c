#include "QSyxOS.h"
#include "q_port.h"
#include "q_task.h"
#include "q_tick.h"
#include "q_timer.h"
#include "q_sched.h"
#include "q_config.h"
#include "stm32f10x.h"

static void qTaskEnd(void)
{
	while (1) {
	}
}

void *QTaskInitStack(uint8_t *stk_ptr, int stk_size, void (*task)(void *arg), void *arg)
{
	uint8_t *p_stk_limit = stk_ptr + stk_size;

	uint32_t *p_stk = (uint32_t *)p_stk_limit;

	*--p_stk = (uint32_t)0x01000000;	/* xPSR */
	*--p_stk = (uint32_t)task;			/* Entry Point */
	*--p_stk = (uint32_t)qTaskEnd;		/* R14(LR) */
	*--p_stk = (uint32_t)0x12121212u;	/* R12 */
	*--p_stk = (uint32_t)0x03030303u;	/* R3 */
	*--p_stk = (uint32_t)0x02020202u;	/* R2 */
	*--p_stk = (uint32_t)p_stk_limit;	/* R1 */
	*--p_stk = (uint32_t)arg;			/* R0 */

	*--p_stk = (uint32_t)0x11111111u;	/* R11 */
	*--p_stk = (uint32_t)0x10101010u;	/* R10 */
	*--p_stk = (uint32_t)0x09090909u;	/* R9  */
	*--p_stk = (uint32_t)0x08080808u;	/* R8  */
	*--p_stk = (uint32_t)0x07070707u;	/* R7  */
	*--p_stk = (uint32_t)0x06060606u;	/* R6  */
	*--p_stk = (uint32_t)0x05050505u;	/* R5  */
	*--p_stk = (uint32_t)0x04040404u;	/* R4  */

	return p_stk;
}

QTask           QSysTickTcb;
uint32_t        QSysTickDlyCtr = 0;
static uint32_t QSysTickStk[128];

static void QSysTickTask(void *arg)
{
	(void)arg;
	CPU_REG reg;
	while (1) {
		while (QSysTickDlyCtr != 0) {
			QSchedLock();

			CPU_CRITICAL_ENTER(reg);
			QSysTickDlyCtr--;
			CPU_CRITICAL_EXIT(reg);

			Q_TaskUpdate();
			Q_TickUpdate();
			Q_TimerUpdate();

			QSchedUnlockNoSched();
		}

		QSchedSwitch();
	}
}

void QSysTickInit(void)
{
	Q_TaskInit(&QSysTickTcb, "SysTick Task", 1, (uint8_t *)QSysTickStk, sizeof(QSysTickStk), 10, QSysTickTask, NULL);

	if (SysTick_Config(SystemCoreClock / CFG_SYSTICK_MAX_HZ)) {
		while (1);
	}

	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
}
