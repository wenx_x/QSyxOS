#ifndef __Q_MBOX_H__
#define __Q_MBOX_H__

#include "QSyxOS.h"

#define Q_MboxFull(mbox)		((mbox)->total == (mbox)->used)
#define Q_MboxEmpty(mbox)		((mbox)->used == 0)

#ifdef CFG_QPOLL_ENABLE
int Q_MboxPollEvent(QMbox *mbox, QPollEvent *ev);
#endif

#endif /* __Q_MBOX_H__ */
