#ifndef __QSYXOS_H__
#define __QSYXOS_H__

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stddef.h>

#include "q_config.h"

#define Q_NO_TIMEOUT	(-1)
#define Q_NO_WAIT		(0)

typedef enum {
	Q_ERR_NONE				= 0,
	Q_ERR_TIMEOUT,
	Q_ERR_INVAL,
	Q_ERR_WOULDBLOCK,

	/* mutex */
	Q_ERR_MUTEX_NOT_OWNER,

	/* mem */
	Q_ERR_MEM_INVALID_BLOCK_SIZE,

	/* Task error code */
	Q_ERR_TASK_INVALID_STATE,
	Q_ERR_TASK_INVALID_PRIO,

	/* Scheduler */
	Q_ERR_SCHED_NOT_RUNNING,

	/* Flag */
	Q_ERR_FLAG_INVALID_MASK,
} QErrno;

typedef struct QListNode {
	struct QListNode *prev;
	struct QListNode *next;
} QListNode;

typedef struct {
	QListNode head;
} QList;

typedef enum {
	QTaskState_Ready   = 0x00,
	QTaskState_Pending = 0x01,
	QTaskState_Waiting = 0x02,
	QTaskState_Suspend = 0x04,
} QTaskState;

typedef enum {
	QPendState_Success = 0,
	QPendState_Started = 1,
	QPendState_Timeout = 2,
} QPendState;

typedef struct {
	int numTasks;
	QList taskLists;
} QPendQueue;

typedef struct {
	QListNode  tickHead;

	uint32_t tickLimits;
	void   (*func)(void *ctx);
	void    *ctx;
} QTimeNode;

typedef struct {
	QTimeNode timeNode;
} QTick;

typedef struct QTask QTask;

struct QTask {
	uint32_t *StkPtr;			/* Current task Stack pointer            */
	int StkSize;				/* Task stack size                       */

	const char *name;			/* Task name                             */

	void (*entry)(void *arg);	/* Task entry point                      */

	int prio;					/* Task current priority                 */
	int originPrio;				/* Task original priority                */

	QListNode   taskNode;		/* Link node for ready/pendQ link list   */
	QTick       tickNode;		/* Pending timeout or time delay         */

	QTaskState  taskState;		/* Current task state                    */
	QPendState  pendState;		/* Current pend state                    */
	QPendQueue *pendQueue;		/* Pend queue currently pending on       */

	/* Round Roubin */
	int tickQuanta;				/* Task time quanta in tick unit         */
	int tickRemain;				/* Task remain tick quanta               */

	/* Event flags */
	int flagsPend;				/* Event flags pending on                */
	int flagsRdy;				/* Event flags that made task ready      */
	int flagsOpt;				/* Event flag option, see QFlagPendOpt   */

	/* MsgQ/MBox */
	void *swapBuf;				/* Swap buffer for MsgQ and Mailbox      */
	int swapSize;				/* Swap buffer size                      */

	int errno;					/* Per-task errno variable               */

	/* QPoll */
#ifdef CFG_QPOLL_ENABLE
	int numReadys;
#endif
};

int *QSyxOSErrPtr(void);
#define Q_ERRNO 	(*QSyxOSErrPtr())

#define CPU_REG	uint32_t

uint32_t QIrqDisable(void);
void QIrqEnable(uint32_t reg);

#define CPU_CRITICAL_ENTER(reg) do { reg = QIrqDisable(); } while (0)
#define CPU_CRITICAL_EXIT(reg)  do { QIrqEnable(reg); } while (0)

void QSyxOSInit(void);
void QSyxOSStart(void);

#ifdef CFG_QPOLL_ENABLE
typedef enum {
	QPollObjType_MsgQ,
	QPollObjType_Mbox,
	QPollObjType_Pipe,
	QPollObjType_Queue,
} QPollObjType;

typedef enum {
	QPollFlag_Read  = 0x1,
	QPollFlag_Write = 0x2,
} QPollFlag;

struct QMsgQ;
struct QMbox;
struct QPipe;
struct QQueue;

typedef struct {
	int ready;

	QTask *poller;
	QPollFlag flag;

	QListNode listNode;

	QPollObjType type;
	union {
		struct QMsgQ *msgq;
		struct QMbox *mbox;
		struct QPipe *pipe;
		struct QQueue *queue;
	} kobj;
} QPollEvent;

typedef struct {
	QList eventList;
} QPollQueue;

int QPoll(QPollEvent *events, int numEvents, int timeout);
#endif

/*
 * QSyxOS Flag
 */
typedef enum {
	QFlagPendOpt_Set_All,
	QFlagPendOpt_Set_Any,
} QFlagPendOpt;

typedef struct {
	int flags;
	QPendQueue pendQ;
} QFlag;

int QFlagInit(QFlag *flag);
int QFlagPend(QFlag *flag, int mask, QFlagPendOpt opt, int timeout);
int QFlagPost(QFlag *flag, int mask);

/*
 * Memory Managment
 */
typedef struct {
	void *first;
	QPendQueue pendQ;
} QMem;

int QMemInit(QMem *mem, void *buf, int numblks, int blksize);
void *QMemGet(QMem *mem, int timeout);
int QMemPut(QMem *mem, void *ptr);

/*
 * Mutex
 */
typedef struct {
	uint32_t refcnt;
	QPendQueue pendQ;

	int holderOrigPrio;
	QTask *holder;
	QListNode listNode;
} QMutex;

int QMutexInit(QMutex *mutex);
int QMutexPend(QMutex *mutex, int timeout);
int QMutexPost(QMutex *mutex);

/*
 * Semaphore
 */
typedef struct {
	uint32_t count;
	QPendQueue pendQ;
} QSem;

int QSemInit(QSem *sem, uint32_t count);
int QSemPend(QSem *sem, int timeout);
int QSemPost(QSem *sem);

/*
 * Message Queue
 */
typedef struct {
	void *data;
} QMsgBlk;

typedef struct QMsgQ {
	uint32_t numBlks;
	QMsgBlk *blks;

	uint32_t rndx;
	uint32_t wndx;

	QPendQueue pendQ;

#ifdef CFG_QPOLL_ENABLE
	QPollQueue pollQ;
#endif
} QMsgQ;

int QMsgQInit(QMsgQ *msgq, QMsgBlk *blks, int numBlks);
int QMsgQPut(QMsgQ *msgq, void *data, int timeout);
void *QMsgQGet(QMsgQ *msgq, int timeout);

int QMsgQFull(QMsgQ *msgq);
int QMsgQEmpty(QMsgQ *msgq);

/*
 * MailBox
 */
typedef struct QMbox {
	uint32_t total;
	uint32_t used;

	uint8_t *buf;
	uint8_t *end;

	uint8_t *wptr;
	uint8_t *rptr;

	uint32_t blksize;

	QPendQueue pendQ;

#ifdef CFG_QPOLL_ENABLE
	QPollQueue pollQ;
#endif
} QMbox;

int QMboxInit(QMbox *mbox, void *buf, int nb_blks, int blksize);
int QMboxPut(QMbox *mbox, void *buf, int timeout);
int QMboxGet(QMbox *mbox, void *buf, int timeout);

/*
 * Task Managment
 */
int QTaskCreate(QTask *task, const char *name, int prio, void *stk_ptr,
		int stk_size, int time_quanta, void (*entry)(void *arg), void *arg);
int QTaskSuspend(QTask *task);
int QTaskResume(QTask *task);
void QTaskYield(void);

/*
 * Time Managment
 */
int QTimeDelay(int tick);
int QTimeAbort(QTask *task);

/*
 * Timer Managment
 */
typedef enum {
	QTimerOpt_OneShot   = 0,
	QTimerOpt_Periodic  = 1,
} QTimerOpt;

typedef struct QTimer {
	uint32_t delay;
	uint32_t period;

	void (*func)(void *ctx);
	void *ctx;

	QTimeNode timeNode;

	QTimerOpt opt;
} QTimer;

int QTimerInit(QTimer *timer, int delay, int period, QTimerOpt opt, void (*func)(void *ctx), void *ctx);
int QTimerStart(QTimer *timer);
int QTimerStop(QTimer *timer);

typedef enum {
	QQueueOpt_Fifo,
	QQueueOpt_Lifo,
} QQueueOpt;

typedef struct QQueue {
	QQueueOpt opt;
	QList allnodes;
	QPendQueue pendQ;

#ifdef CFG_QPOLL_ENABLE
	QPollQueue pollQ;
#endif
} QQueue;

typedef struct {
	QListNode listNode;
} QQueueNode;

int QQueueInit(QQueue *queue, QQueueOpt opt);
int QQueuePut(QQueue *queue, QQueueNode *node);
QQueueNode *QQueueGet(QQueue *queue, int timeout);

#define QQueueEntry(ptr, type, member) 	(type *)((char *)ptr - offsetof(type, member))

typedef struct QPipe {
	int size;
	int used;

	uint8_t *buf;
	uint8_t *end;

	uint8_t *wptr;
	uint8_t *rptr;

	QPendQueue pendQ;

#ifdef CFG_QPOLL_ENABLE
	QPollQueue pollQ;
#endif
} QPipe;

int QPipeInit(QPipe *pipe, void *buf, int size);
int QPipeGet(QPipe *pipe, void *buf, int size, int timeout);
int QPipePut(QPipe *pipe, void *buf, int size, int timeout);

#endif /* __QSYXOS_H__ */
