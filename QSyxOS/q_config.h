#ifndef __Q_CONFIG_H__
#define __Q_CONFIG_H__

#define CFG_TIMEWHEEL_MAX_SPOKE    (17)

#define CFG_SYSTICK_MAX_HZ         (1000)

#define CFG_TIMEQUANTA_DEFAULT     (10)

#define CPU_REG                    uint32_t

#define CFG_PRIO_MAX_NUM           (32)
#define CFG_BITS_MAX_SLOT          ((CFG_PRIO_MAX_NUM - 1) / (sizeof(uint32_t) * 8) + 1)

#define CFG_PRIO_HIGHEST           (0)
#define CFG_PRIO_LOWEST            (CFG_PRIO_MAX_NUM - 1)

#define CFG_TASK_PRIO_TIMER        CFG_PRIO_HIGHEST
#define CFG_TASK_PRIO_IDLE         CFG_PRIO_LOWEST

#define CFG_BITS_PER_UINT32        (32)

#define CFG_QPOLL_ENABLE

#endif /* __Q_CONFIG_H__ */
