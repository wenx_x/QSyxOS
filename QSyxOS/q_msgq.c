#include "QSyxOS.h"
#include "q_task.h"
#include "q_msgq.h"
#include "q_poll.h"
#include "q_sched.h"
#include "q_pendq.h"
#include "q_config.h"

int QMsgQEmpty(QMsgQ *msgq)
{
	CPU_REG reg;
	CPU_CRITICAL_ENTER(reg);
	int empty = Q_MsgQEmpty(msgq);
	CPU_CRITICAL_EXIT(reg);

	return empty;
}

int QMsgQFull(QMsgQ *msgq)
{
	CPU_REG reg;
	CPU_CRITICAL_ENTER(reg);
	int full = Q_MsgQFull(msgq);
	CPU_CRITICAL_EXIT(reg);

	return full;
}

int QMsgQInit(QMsgQ *msgq, QMsgBlk *blks, int numBlks)
{
	msgq->numBlks = numBlks;
	msgq->blks = blks;
	msgq->rndx = msgq->wndx = 0;

	Q_PendQueueInit(&msgq->pendQ);
#ifdef CFG_QPOLL_ENABLE
	Q_PollQueueInit(&msgq->pollQ);
#endif

	int i;
	for (i = 0; i < numBlks; i++) {
		blks[i].data = NULL;
	}

	return 0;
}

inline static void Q_MsgQPut(QMsgQ *msgq, void *data)
{
	msgq->blks[msgq->wndx % msgq->numBlks].data = data;
	msgq->wndx++;
}

int QMsgQPut(QMsgQ *msgq, void *data, int timeout)
{
	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();
	if (!Q_MsgQFull(msgq)) {
		QTask *task = Q_PendQueueFirst(&msgq->pendQ);
		if (task) {
#ifdef CFG_QPOLL_ENABLE
			QPollEvent *ev = Q_PollQueueGetHighest(&msgq->pollQ);
			if (ev && ev->poller->prio > task->prio) {
				Q_MsgQPut(msgq, data);
				Q_PollSignal(ev, QPollFlag_Read);
			} else
#endif
			{
				task->swapBuf = data;
				Q_PendQueuePost(&msgq->pendQ, task);
			}
#ifdef CFG_QPOLL_ENABLE
		} else {
			Q_MsgQPut(msgq, data);
			QPollEvent *ev = Q_PollQueueGetHighest(&msgq->pollQ);
			if (ev)
				Q_PollSignal(ev, QPollFlag_Read);
#endif
		}
		QSchedUnlock();
		return 0;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return -1;
	}

	QTaskCurPtr->swapBuf = data;
	if (Q_PendQueuePend(&msgq->pendQ, timeout, 1) < 0) {
		QSchedUnlock();
		return -1;
	}
	QSchedUnlock();

	return 0;
}

inline static void *Q_MsgQGet(QMsgQ *msgq)
{
	void *data = msgq->blks[msgq->rndx % msgq->numBlks].data;
	msgq->rndx++;
	return data;
}

void *QMsgQGet(QMsgQ *msgq, int timeout)
{
	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return NULL;
	}

	QSchedLock();
	if (!Q_MsgQEmpty(msgq)) {
		void *data = Q_MsgQGet(msgq);

		QTask *task = Q_PendQueueFirst(&msgq->pendQ);
		if (task) {
#ifdef CFG_QPOLL_ENABLE
			QPollEvent *ev = Q_PollQueueGetHighest(&msgq->pollQ);
			if (ev && ev->poller->prio > task->prio) {
				Q_PollSignal(ev, QPollFlag_Write);
			} else
#endif
			{
				Q_MsgQPut(msgq, task->swapBuf);
				Q_PendQueuePost(&msgq->pendQ, task);
			}
#ifdef CFG_QPOLL_ENABLE
		} else {
			QPollEvent *ev = Q_PollQueueGetHighest(&msgq->pollQ);
			if (ev)
				Q_PollSignal(ev, QPollFlag_Write);
#endif
		}
		QSchedUnlock();
		return data;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return NULL;
	}

	if (Q_PendQueuePend(&msgq->pendQ, timeout, 1) < 0) {
		QSchedUnlock();
		return NULL;
	}
	QSchedUnlock();

	return QTaskCurPtr->swapBuf;
}

#ifdef CFG_QPOLL_ENABLE
int Q_MsgQPollEvent(QMsgQ *msgq, QPollEvent *ev)
{
	int needwait = 0;
	switch (ev->flag) {
	case QPollFlag_Read:
		needwait = Q_MsgQEmpty(msgq);
		break;
	case QPollFlag_Write:
		needwait = Q_MsgQFull(msgq);
		break;
	}

	if (needwait) {
		Q_PollQueueInsert(&msgq->pollQ, ev);
	} else {
		ev->ready = 1;
		ev->poller->numReadys++;
	}

	return 0;
}
#endif
