#include "QSyxOS.h"
#include "q_tick.h"
#include "q_list.h"
#include "q_timeq.h"
#include "q_config.h"

static uint32_t QSysTickCtr = 0;

static QTimeQueue timeq;

void Q_TickInitModule(void)
{
	Q_TimeQueueInit(&timeq);
}

void Q_TickInit(QTick *tick)
{
	tick->timeNode.tickLimits = 0;
	tick->timeNode.func       = NULL;
	tick->timeNode.ctx        = NULL;
}

void Q_TickPend(QTick *tick, int ticks, void (*tickFunc)(void *ctx), void *ctx)
{
	if (ticks > 0) {
		tick->timeNode.func       = tickFunc;
		tick->timeNode.ctx        = ctx;
		tick->timeNode.tickLimits = QSysTickCtr + ticks;

		Q_TimeQueueInsert(&timeq, &tick->timeNode);
	}
}

void Q_TickPost(QTick *tick)
{
	tick->timeNode.tickLimits = 0;
	tick->timeNode.func       = NULL;
	tick->timeNode.ctx        = NULL;

	Q_TimeQueueRemove(&timeq, &tick->timeNode);
}

void Q_TickUpdate(void)
{
	Q_TimeQueueUpdate(&timeq, QSysTickCtr);
	QSysTickCtr++;
}
