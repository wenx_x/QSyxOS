#include "QSyxOS.h"
#include "q_list.h"
#include "q_sched.h"
#include "q_timeq.h"
#include "q_config.h"

static uint32_t QTimerTickCtr = 0;

static QSem timerSem;
static QTask timerTask;
static uint32_t timerStk[256];

static QTimeQueue timeq;

static void Q_TimerTask(void *arg);

void Q_TimerInitModule(void)
{
	QSemInit(&timerSem, 0);
	Q_TimeQueueInit(&timeq);
	QTaskCreate(&timerTask, "QSyxOS Timer Task", CFG_TASK_PRIO_TIMER, (uint8_t *)timerStk, sizeof(timerStk), 0, Q_TimerTask, NULL);
}

static void timerExpired(void *ctx)
{
	QTimer *timer = (QTimer *)ctx;

	if (timer->func)
		timer->func(timer->ctx);

	if (timer->opt == QTimerOpt_Periodic) {
		timer->timeNode.tickLimits = QTimerTickCtr + timer->period;
		Q_TimeQueueInsert(&timeq, &timer->timeNode);
	}
}

int QTimerInit(QTimer *timer, int delay, int period, QTimerOpt opt, void (*func)(void *ctx), void *ctx)
{
	if (delay < 0 || period <= 0 || !func) {
		Q_ERRNO = Q_ERR_INVAL;
		return -1;
	}

	timer->delay  = delay;
	timer->period = period;
	timer->opt    = opt;
	timer->func   = func;
	timer->ctx    = ctx;

	timer->timeNode.func = timerExpired;
	timer->timeNode.ctx  = timer;

	return 0;
}

int QTimerStart(QTimer *timer)
{
	QSchedLock();
	timer->timeNode.tickLimits = QTimerTickCtr + timer->delay;
	Q_TimeQueueInsert(&timeq, &timer->timeNode);
	QSchedUnlock();

	return 0;
}

int QTimerStop(QTimer *timer)
{
	QSchedLock();
	Q_TimeQueueRemove(&timeq, &timer->timeNode);
	QSchedUnlock();

	return 0;
}

static void Q_TimerTask(void *arg)
{
	(void)arg;
	while (1) {
		QSemPend(&timerSem, Q_NO_TIMEOUT);
		QSchedLock();
		Q_TimeQueueUpdate(&timeq, QTimerTickCtr);
		QTimerTickCtr++;
		QSchedUnlock();
	}
}

void Q_TimerUpdate(void)
{
	QSemPost(&timerSem);
}
