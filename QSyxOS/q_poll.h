#ifndef __Q_POLL_H__
#define __Q_POLL_H__

#ifdef CFG_QPOLL_ENABLE

void Q_PollQueueInit(QPollQueue *pollQ);

int Q_PollSignal(QPollEvent *event, QPollFlag flag);

QPollEvent *Q_PollQueueGetHighest(QPollQueue *pollQ);

int Q_PollQueueInsert(QPollQueue *pollQ, QPollEvent *ev);

#endif /* CFG_QPOLL_ENABLE */

#endif /* __Q_POLL_H__ */
