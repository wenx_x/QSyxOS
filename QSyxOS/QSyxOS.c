#include "QSyxOS.h"
#include "q_task.h"
#include "q_time.h"
#include "q_tick.h"
#include "q_port.h"
#include "q_timer.h"
#include "q_sched.h"

void QSyxOSInit(void)
{
	Q_TaskInitModule();
	Q_TickInitModule();
	Q_TimerInitModule();
}

void QSyxOSStart(void)
{
	QSysTickInit();
	QSchedStart();
}

CPU_REG QIrqDisable(void)
{
	return IrqDisable();
}

void QIrqEnable(CPU_REG reg)
{
	IrqEnable(reg);
}

//static int QIntErrno = 0;

int *QSyxOSErrPtr(void)
{
#if 0
	if (is_in_isr())
		return &QIntErrno;
#endif
	return &QTaskCurPtr->errno;
}
