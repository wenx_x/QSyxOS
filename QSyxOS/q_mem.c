#include "QSyxOS.h"
#include "q_task.h"
#include "q_sched.h"
#include "q_pendq.h"

int QMemInit(QMem *mem, void *buf, int numblks, int blksize)
{
	if (blksize < (int)sizeof(uint8_t *)) {
		Q_ERRNO = Q_ERR_MEM_INVALID_BLOCK_SIZE;
		return -1;
	}

	mem->first = NULL;

	uint8_t *ptr = buf;

	int i;
	for (i = 0; i < numblks; i++) {
		*(uint8_t **)ptr = mem->first;
		mem->first = ptr;
		ptr += blksize;
	}

	return 0;
}

void *QMemGet(QMem *mem, int timeout)
{
	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return NULL;
	}

	QSchedLock();
	if (mem->first != NULL) {
		uint8_t *ptr = mem->first;
		mem->first = *(uint8_t **)ptr;
		QSchedUnlock();
		return ptr;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return NULL;
	}

	if (Q_PendQueuePend(&mem->pendQ, timeout, 1) < 0) {
		QSchedUnlock();
		return NULL;
	}
	QSchedUnlock();

	return QTaskCurPtr->swapBuf;
}

int QMemPut(QMem *mem, void *ptr)
{
	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();
	QTask *task = Q_PendQueueFirst(&mem->pendQ);
	if (task) {
		task->swapBuf = ptr;
		Q_PendQueuePost(&mem->pendQ, task);
	} else {
		*(uint8_t **)ptr = mem->first;
		mem->first = ptr;
	}
	QSchedUnlock();

	return 0;
}
