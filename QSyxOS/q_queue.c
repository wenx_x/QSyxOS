#include "QSyxOS.h"
#include "q_list.h"
#include "q_task.h"
#include "q_poll.h"
#include "q_pendq.h"
#include "q_queue.h"
#include "q_sched.h"

int QQueueInit(QQueue *queue, QQueueOpt opt)
{
	queue->opt = opt;
	Q_ListInit(&queue->allnodes);
	Q_PendQueueInit(&queue->pendQ);
#ifdef CFG_QPOLL_ENABLE
	Q_PollQueueInit(&queue->pollQ);
#endif
	return 0;
}

inline static int Q_QueuePut(QQueue *queue, QQueueNode *node)
{
	switch (queue->opt) {
	case QQueueOpt_Fifo:
		Q_ListAppend(&queue->allnodes, &node->listNode);
		break;
	case QQueueOpt_Lifo:
		Q_ListPrepend(&queue->allnodes, &node->listNode);
		break;
	}

	return 0;
}

int QQueuePut(QQueue *queue, QQueueNode *node)
{
	Q_ERRNO = Q_ERR_NONE;

	QSchedLock();

	QTask *task = Q_PendQueueFirst(&queue->pendQ);
	if (task) {
#ifdef CFG_QPOLL_ENABLE
		QPollEvent *ev = Q_PollQueueGetHighest(&queue->pollQ);
		if (ev && ev->poller->prio > task->prio) {
			Q_QueuePut(queue, node);
			Q_PollSignal(ev, QPollFlag_Read);
		} else
#endif
		{
			task->swapBuf = node;
			Q_PendQueuePost(&queue->pendQ, task);
		}
#ifdef CFG_QPOLL_ENABLE
	} else {
		Q_QueuePut(queue, node);
		QPollEvent *ev = Q_PollQueueGetHighest(&queue->pollQ);
		if (ev)
			Q_PollSignal(ev, QPollFlag_Read);
#endif
	}

	QSchedUnlock();

	return 0;
}

inline static QQueueNode *Q_QueueGet(QQueue *queue)
{
	QListNode *listNode = NULL;
	switch (queue->opt) {
	case QQueueOpt_Fifo:
		listNode = Q_ListLast(&queue->allnodes);
		break;
	case QQueueOpt_Lifo:
		listNode = Q_ListFirst(&queue->allnodes);
		break;
	}
	return Q_ListEntry(listNode, QQueueNode, listNode);
}

QQueueNode *QQueueGet(QQueue *queue, int timeout)
{
	Q_ERRNO = Q_ERR_NONE;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return NULL;
	}

	QSchedLock();
	if (!Q_ListEmpty(&queue->allnodes)) {
		QQueueNode *node = Q_QueueGet(queue);
		Q_ListRemove(&node->listNode);
		QSchedUnlock();
		return node;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return NULL;
	}

	if (Q_PendQueuePend(&queue->pendQ, timeout, 1) < 0) {
		QSchedUnlock();
		return NULL;
	}
	QSchedUnlock();

	return (QQueueNode *)QTaskCurPtr->swapBuf;
}

#ifdef CFG_QPOLL_ENABLE
int Q_QueuePollEvent(QQueue *queue, QPollEvent *ev)
{
	int needwait = 0;
	switch (ev->flag) {
	case QPollFlag_Read:
		needwait = Q_QueueEmpty(queue);
		break;
	case QPollFlag_Write:
		needwait = 0;	/* always no need to wait */
		break;
	}

	if (needwait) {
		Q_PollQueueInsert(&queue->pollQ, ev);
	} else {
		ev->ready = 1;
		ev->poller->numReadys++;
	}

	return 0;
}
#endif
