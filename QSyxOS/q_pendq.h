#ifndef __Q_PENDQ_H__
#define __Q_PENDQ_H__

#include "QSyxOS.h"
#include "q_list.h"
#include "q_tick.h"

void Q_PendQueueInit(QPendQueue *q);

int Q_PendQueuePend(QPendQueue *q, int ticks, int schedLocked);
int Q_PendQueuePost(QPendQueue *q, QTask *task);

inline static QTask *Q_PendQueueFirst(QPendQueue *q)
{
	if (q->numTasks == 0)
		return NULL;

	return Q_ListFirstEntry(&q->taskLists, QTask, taskNode);
}

inline static QTask *Q_PendQueueNext(QPendQueue *q, QTask *task)
{
	if (q->numTasks == 0)
		return NULL;

	if (Q_ListIsLast(&q->taskLists, &task->taskNode))
		return NULL;

	return Q_ListNextEntry(task, QTask, taskNode);
}

inline static QTask *Q_PendQueueGetHightest(QPendQueue *q)
{
	return Q_PendQueueFirst(q);
}

int Q_PendQueueUpdatePrio(QTask *task, int prio);

#endif /* __Q_PENDQ_H__ */
