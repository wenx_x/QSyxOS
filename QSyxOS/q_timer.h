#ifndef __Q_TIMER_H__
#define __Q_TIMER_H__

void Q_TimerInitModule(void);

void Q_TimerUpdate(void);

#endif /* __Q_TIMER_H__ */
