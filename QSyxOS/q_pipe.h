#ifndef __Q_PIPE_H__
#define __Q_PIPE_H__

#include "QSyxOS.h"

#define Q_PipeUsed(pipe)	((pipe)->used)
#define Q_PipeFree(pipe)	((pipe)->size - (pipe)->used)
#define Q_PipeEmpty(pipe)	((pipe)->used == 0)
#define Q_PipeFull(pipe)	((pipe)->used == (pipe)->size)

#ifdef CFG_QPOLL_ENABLE
int Q_PipePollEvent(QPipe *pipe, QPollEvent *ev);
#endif

#endif /* __Q_PIPE_H__ */
