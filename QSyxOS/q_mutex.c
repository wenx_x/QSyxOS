#include "QSyxOS.h"
#include "q_list.h"
#include "q_task.h"
#include "q_pendq.h"
#include "q_sched.h"

int QMutexInit(QMutex *mutex)
{
	Q_PendQueueInit(&mutex->pendQ);
	mutex->refcnt = 0;
	mutex->holder = NULL;
	mutex->holderOrigPrio = 0;
	return 0;
}

int QMutexPend(QMutex *mutex, int timeout)
{
	Q_ERRNO = Q_ERR_NONE;
	QTask *task = QTaskCurPtr;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();
	if (!mutex->holder) {
		mutex->holder = task;
		mutex->refcnt++;
		mutex->holderOrigPrio = task->prio;
		QSchedUnlock();
		return 0;
	}

	if (mutex->holder == task) {
		mutex->refcnt++;
		QSchedUnlock();
		return 0;
	}

	if (timeout == Q_NO_WAIT) {
		Q_ERRNO = Q_ERR_WOULDBLOCK;
		QSchedUnlock();
		return -1;
	}

	/* priority inheritance */
	if (Q_TaskPrioIsHigher(task->prio, mutex->holder->prio))
		Q_TaskUpdatePrio(mutex->holder, task->prio);

	if (Q_PendQueuePend(&mutex->pendQ, timeout, 1) < 0) {
		/* restore mutex holder's priority */
		QTask *highest = Q_PendQueueGetHightest(&mutex->pendQ);
		if (highest->prio != mutex->holder->prio)
			Q_TaskUpdatePrio(mutex->holder, highest->prio);
		QSchedUnlock();
		return -1;
	}
	QSchedUnlock();

	return 0;
}

int QMutexPost(QMutex *mutex)
{
	Q_ERRNO = Q_ERR_NONE;
	QTask *task = QTaskCurPtr;

	if (!Q_SchedRunning()) {
		Q_ERRNO = Q_ERR_SCHED_NOT_RUNNING;
		return -1;
	}

	QSchedLock();
	/* only holder can release the mutex */
	if (mutex->holder != task) {
		Q_ERRNO = Q_ERR_MUTEX_NOT_OWNER;
		QSchedUnlock();
		return -1;
	}

	if (--mutex->refcnt > 0) {
		Q_ERRNO = Q_ERR_NONE;
		QSchedUnlock();
		return mutex->refcnt;
	}

	/* restore priority */
	if (mutex->holderOrigPrio != task->prio)
		Q_TaskUpdatePrio(task, mutex->holderOrigPrio);

	QTask *new_owner = Q_PendQueueFirst(&mutex->pendQ);
	if (new_owner) {
		mutex->holder = new_owner;
		mutex->refcnt++;
		mutex->holderOrigPrio = new_owner->prio;
		Q_PendQueuePost(&mutex->pendQ, new_owner);
	} else {
		mutex->holder = NULL;
	}

	QSchedUnlock();	/* schedule here */

	return 0;
}
