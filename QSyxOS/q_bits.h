#ifndef __Q_BITS_H__
#define __Q_BITS_H__

#include "QSyxOS.h"
#include "q_port.h"
#include "q_config.h"

#define WORD_IDX(n)	((n) >> 5)
#define BITS_IDX(n)	(CFG_BITS_PER_UINT32 - ((n) & 0x1f) - 1)

typedef struct {
	int nwords;
	uint32_t *words;
} QBits;

inline static int Q_BitsInit(QBits *bits, uint32_t *words, int nwords)
{
	int i;
	for (i = 0; i < nwords; i++)
		words[i] = 0;

	bits->words  = words;
	bits->nwords = nwords;

	return 0;
}

#define Q_BitsSet(bits, n) do { (bits)->words[WORD_IDX(n)] |=  (1 << BITS_IDX(n)); } while (0)
#define Q_BitsClr(bits, n) do { (bits)->words[WORD_IDX(n)] &= ~(1 << BITS_IDX(n)); } while (0)

inline static int Q_BitsGetHighest(QBits *bits)
{
	int n;
	for (n = 0; n < bits->nwords; n++) {
		if (bits->words[n])
			return (n << 5) + CPU_CntLeadZeros(bits->words[n]);
	}

	return -1;
}

#endif /* __Q_BITS_H__ */
