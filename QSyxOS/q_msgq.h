#ifndef __Q_MSGQ_H__
#define __Q_MSGQ_H__

#include "QSyxOS.h"

#define Q_MsgQUsed(msgq) 	((uint32_t)((msgq)->wndx - (msgq)->rndx))
#define Q_MsgQEmpty(msgq)	(Q_MsgQUsed(msgq) == 0)
#define Q_MsgQFull(msgq)	(Q_MsgQUsed(msgq) == (msgq)->numBlks)

#ifdef CFG_QPOLL_ENABLE
int Q_MsgQPollEvent(QMsgQ *msgq, QPollEvent *ev);
#endif

#endif /* __Q_MSGQ_H__ */
