#include "bsp_led.h"

#include "QSyxOS.h"
#include "q_task.h"
#include "q_tick.h"
#include "q_time.h"
#include "q_config.h"
#include "bsp_usart.h"

static QSem led1Sem;
static QSem led2Sem;

static QTask lowTcb;
static QTask led1Tcb;
static QTask led2Tcb;

static uint32_t lowStk[256];
static uint32_t led1Stk[256];
static uint32_t led2Stk[256];

void lowTsk(void *arg)
{
	while (1) {
		QSemPost(&led1Sem);
		QTimeDelay(100);
		QSemPost(&led2Sem);
		QTimeDelay(100);
	}
}

void led1Tsk(void *arg)
{
	int count = 0;
	while (1) {
		QSemPend(&led1Sem, Q_NO_TIMEOUT);
		if (count++ % 2 == 0)
			macLED1_ON();
		else
			macLED1_OFF();
	}
}

void led2Tsk(void *arg)
{
	int count = 0;
	while (1) {
		QSemPend(&led2Sem, Q_NO_TIMEOUT);
		if (count++ % 2 == 0)
			macLED2_ON();
		else
			macLED2_OFF();
	}
}

int main(void)
{
	LED_Init();
	QSyxOSInit();
	USART_Config();
//	printf("message from rtos\n");

	QSemInit(&led1Sem, 0);
	QSemInit(&led2Sem, 0);

	QTaskCreate(&lowTcb,  "Low Task",  1, (uint8_t *)lowStk,  sizeof(lowStk),  8, lowTsk,  NULL);
	QTaskCreate(&led1Tcb, "Led1 Task", 8, (uint8_t *)led1Stk, sizeof(led1Stk), 7, led1Tsk, NULL);
	QTaskCreate(&led2Tcb, "Led2 Task", 5, (uint8_t *)led2Stk, sizeof(led2Stk), 6, led2Tsk, NULL);

	QSyxOSStart();

	return 0;
}

/*********************************************END OF FILE**********************/
